"""
This is the primary file that should be ran directly.
"""

# needed to check if the program can run
import integrityCheck
# needed to perform website upgrades
import systemIO
# needed to fetch the polling delay
import configEngine
# needed to wait before upgrading the website
import time

# container to store polling delay
POLL_DELAY = 0


def init():
    """
    This function checks that the program can run and sets the polling delay.
    """
    # needed in order to set the polling delay variable
    global POLL_DELAY
    # check if program can run
    if not integrityCheck.checkEnvironment():
        writeLog("This program has unmet dependencies and can not run")
        exit()
    # check if local git repo exists
    systemIO.checkGitExists()
    # get the polling delay
    POLL_DELAY = configEngine.getPollDelay()


def run():
    """
    This function checks for updates at regular time intervals.
    """
    # enter main loop
    while True:
        # attempt to update
        systemIO.checkGitUpdate()
        # wait for a specific amount of time
        time.sleep(POLL_DELAY)


def main():
    """
    THis function initializes the program and calls the main loop.
    """
    # initialize the program
    writeLog("Program is starting")
    init()
    # run the program
    writeLog("Program has started")
    run()
    # stop the program
    writeLog("Program stopped")
    exit()


def writeLog(text):
    """
    This function prints a string to the terminal in a precise format.

    Args:
        text (str): the text wished to be printed
    """
    print("[Main] %s." % text)


# call main function
main()
