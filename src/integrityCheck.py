import os
import sys
import subprocess


def checkEnvironment():
    """
    This function checks the software environment and determines if the program can run.

    Returns:
        bool: true if the program can run safely
    """
    # TEST 1: System is Linux
    # TEST 2: Program is ran by root
    # TEST 3: Git is installed
    # TEST 4: Systemd is installed
    # TEST 5: Nginx is installed
    # TEST 6: Nginx unit is installed
    tests = [
        isLinux,
        isRoot,
        hasGit,
        hasSystemd,
        hasNginx,
        hasSystemdUnitNginx
    ]
    for test in tests:
        status = test()
        if status:
            writeLog("Test %s passed" % (test.__name__))
        else:
            writeLog("Test %s failed" % (test.__name__))
            writeLog("All tests must pass in order to run this program")
            return False
    writeLog("All tests passed")
    return True


def hasGit():
    """
    This function checks if git is installed.

    Returns:
        bool: true if git is in $PATH
    """
    if programExists("git"):
        writeLog("Git is installed")
        return True
    else:
        writeLog("Git is not installed")
        return False


def hasSystemd():
    """
    This function checks if systemd is installed.

    Returns:
        bool: true if systemctl is in $PATH
    """
    if programExists("systemctl"):
        writeLog("Systemd is installed")
        return True
    else:
        writeLog("Systemd is not installed")
        return False


def hasNginx():
    """
    This function checks if nginx is installed.

    Returns:
        bool: true if nginx is in $PATH
    """
    if programExists("nginx"):
        writeLog("Nginx is installed")
        return True
    else:
        writeLog("Nginx is not installed")
        return False


def isRoot():
    """
    This function checks if the program is being ran as root.

    Returns:
        bool: true if $USER is root
    """
    if os.getenv("USER") == "root":
        writeLog("The user is root")
        return True
    else:
        writeLog("The user is not root")
        return False


def isLinux():
    """
    This function checks if the operating system is Linux.

    Returns:
        bool: true if the operating system is Linux
    """
    if sys.platform == "linux":
        writeLog("The operating system is Linux")
        return True
    else:
        writeLog("The operating system is not Linux")
        return False


def hasSystemdUnitNginx():
    """
    This function checks if the nginx systemd unit is installed.

    Raises:
        ValueError: systemctl terminates with an unexpected exit code

    Returns:
        bool: true if systemctl can provide the status of the nginx unit.
    """
    p = subprocess.run(["systemctl", "status", "nginx.service", "--no-pager", "--lines", "0"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    code = p.returncode
    # explanation of this return code logic: https://stackoverflow.com/questions/56719780/what-return-code-does-systemctl-status-return-for-an-error-in-systemctl-status
    if code >= 0 and code <= 3:
        writeLog("Systemd found the nginx service")
        return True
    elif code == 4:
        writeLog("Systemd could not find the nginx service")
        return False
    else:
        raise ValueError("There is no logic to handle status code %d." % code)


def programExists(program):
    """
    This function checks if a program can be found in $PATH

    Args:
        program (str): the name of a program's executable file

    Returns:
        bool: true if the program exists in $PATH
    """
    # this code is modifed from https://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
    import os

    def is_exec(testPath):
        """
        This function checks if a given path is an executable.

        Args:
            testPath (str): a filesystem path

        Returns:
            bool: true if the path is a file and can be executed
        """
        return os.path.isfile(testPath) and os.access(testPath, os.X_OK)

    for path in os.environ["PATH"].split(os.pathsep):
        exec_file = os.path.join(path, program)
        if is_exec(exec_file):
            return True
    return False


def writeLog(text):
    """
    This function prints a string to the terminal in a precise format.

    Args:
        text (str): the text wished to be printed
    """
    print("[Integrity Check] %s." % text)


def __test__():
    """
    This function calls each test in this file and counts the number of tests passed and failed.
    """
    passedTests = 0
    failedTests = 0

    print("========================")
    print("===| Testing hasGit |===")
    print("========================")
    result = hasGit()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("=========================")
    print("===| Testing isLinux |===")
    print("=========================")
    result = isLinux()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("========================")
    print("===| Testing isRoot |===")
    print("========================")
    result = isRoot()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("=====================================")
    print("===| Testing hasSystemdUnitNginx |===")
    print("=====================================")
    result = hasSystemdUnitNginx()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("==========================")
    print("===| Testing hasNginx |===")
    print("==========================")
    result = hasNginx()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("===========================")
    print("===| Testing hasSystemd |==")
    print("===========================")
    result = hasSystemd()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("=================================")
    print("===| Testing checkEnvironment |==")
    print("=================================")
    result = checkEnvironment()
    print("The test ", end="")
    if result:
        print("passed.")
        passedTests += 1
    else:
        print("failed.")
        failedTests += 1

    print("=====================")
    print("===| Test Results |==")
    print("=====================")
    print("%d tests passed." % passedTests)
    print("%d tests failed." % failedTests)
    print("%.2f%% passed." % (passedTests/(passedTests+failedTests) * 100))
